{-# LANGUAGE OverloadedStrings #-}


import Data.Vin.Decoder ( decode )
import Control.Monad.Except ( runExceptT )

main :: IO ()
main = do
    -- MPATFS77H6H533890 isuzu dmax
    vin <- runExceptT $ decode "MPATFS77H6H533890"
    print vin