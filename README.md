# hs-vin-decoder

Vehicle Identification Number decoder written entirely in Haskell.

Sources:
- Isuzu:
- https://www.pdf-archive.com/2016/05/04/isuzu-vin/
- https://vpic.nhtsa.dot.gov/mid/home/displayfile/373095e8-7987-4b00-9f01-15dce143361b