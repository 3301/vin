{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}

module Data.Vin.Decoder(
    decode
) where

import Prelude hiding (head)
import Data.Text(Text)
import qualified Data.Text as T
import qualified Data.Vin.Types as VT
import Data.Map(Map)
import qualified Data.Map as M
import Control.Monad.Except



defaultVinLength :: Int
defaultVinLength = 17

decode :: Text -> ExceptT VT.VehicleIdentificationError IO VT.VehicleIdentificationNumber
decode vinAsText = if T.length vinAsText /= defaultVinLength then throwError VT.InvalidVinLength else do
    wmiDecoded <- decodeWmi VT.wmiData (T.take 3 vinAsText)
    vdsDecoded <- decodeVds vinAsText

    pure $ VT.VehicleIdentificationNumber (uncurry VT.WorldManufacturerIdentifier wmiDecoded) (VT.VehicleDescriptorSection vdsDecoded) (VT.VehicleIdentifierSection "")
    where
        decodeWmi :: [(VT.Wmi, (VT.Country, VT.Manufacturer))] -> Text -> ExceptT VT.VehicleIdentificationError IO (VT.Country, VT.Manufacturer)
        decodeWmi wmiData wmiAsText = head $ filter (\e -> wmiAsText == VT.unWmi (fst e)) wmiData

        head :: [(VT.Wmi, (VT.Country, VT.Manufacturer))] -> ExceptT VT.VehicleIdentificationError IO (VT.Country, VT.Manufacturer)
        head = \case
                      x:_ -> pure $ snd x
                      _ -> throwError VT.InvalidWmiIdentifier

        decodeVds :: Text -> ExceptT VT.VehicleIdentificationError IO (Map Text Text)
        decodeVds v = case (T.take 3 v, T.take 6 $ T.drop 3 v) of 
            ("MPA", vds) -> do
                            let d1 = case T.head vds of
                                       'B' -> M.insert "Brake system" "Hydraulic" M.empty
                                       'C' -> M.insert "Brake system" "Hydraulic" M.empty
                                       'E' -> M.insert "Brake system" "Hydraulic" M.empty
                                       'K' -> M.insert "Brake system" "Air" M.empty
                                       'M' -> M.insert "Brake system" "Air" M.empty
                                       'T' -> M.insert "Brake system" "Hydraulic" M.empty -- TODO: throw error ?
                                       _ -> M.empty

                            pure d1

            _ -> pure M.empty
