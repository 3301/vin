{-# LANGUAGE OverloadedStrings #-}

module Data.Vin.Types (
    VehicleIdentificationNumber(..)
  , WorldManufacturerIdentifier(..)
  , VehicleDescriptorSection(..)
  , VehicleIdentifierSection(..)
  , VehicleIdentificationError(..)
  , wmiData
  , Wmi(..)
  , Country(..)
  , Manufacturer(..)
) where

import Data.Text(Text)
import Data.Map


data VehicleIdentificationError = InvalidVinLength | InvalidWmiIdentifier deriving (Show)


-- data types following ISO-3779
data WorldManufacturerIdentifier = WorldManufacturerIdentifier Country Manufacturer deriving (Eq, Show)


newtype Wmi = Wmi { unWmi :: Text } deriving (Eq, Show)
newtype Country = Country { unCountry :: Text } deriving (Eq, Show)
newtype Manufacturer = Manufacturer { unManufacturer :: Text } deriving (Eq, Show)

wmiData :: [(Wmi, (Country, Manufacturer))]
wmiData = [
            (Wmi "JAC", (Country "Japan", Manufacturer "Isuzu SUV"))
          , (Wmi "MPA", (Country "Thailand", Manufacturer "IMCT Isuzu Motors Company Thailand"))
          , (Wmi "SAA", (Country "United Kingdom", Manufacturer "Austin"))
          , (Wmi "SAB", (Country "United Kingdom", Manufacturer "Optare"))
          , (Wmi "SAD", (Country "United Kingdom", Manufacturer "Jaguar SUV")) -- SAD 	Daimler until April 1987
          , (Wmi "SAF", (Country "United Kingdom", Manufacturer "ERF"))
          , (Wmi "SAH", (Country "United Kingdom", Manufacturer "Honda made by Austin Rover Group"))
          , (Wmi "SAJ", (Country "United Kingdom", Manufacturer "Jaguar"))
          , (Wmi "SHH", (Country "United Kingdom", Manufacturer "Honda UK Manufacturing car"))
          
          ]

-- Vehicle Description Section is manufacturer specific 
newtype VehicleDescriptorSection = VehicleDescriptorSection (Map Text Text) deriving (Eq, Show)

data VehicleIdentifierSection = VehicleIdentifierSection Text deriving (Eq, Show)

data VehicleIdentificationNumber = VehicleIdentificationNumber {
    wmi :: WorldManufacturerIdentifier
  , vds :: VehicleDescriptorSection
  , vis :: VehicleIdentifierSection
} deriving (Eq, Show)



